'''@file                mainpage.py
   @brief               This file creates the main page for the project.
   @details             This file contains the text to generate the proper structure for the main page of the project. 

   @mainpage

   @section sec_intro   Introduction
                        This project creates a code that allows a user to display three different LED patterns using a button to cycle through the displays.
                        The three patterns are a square wave pattern, sine wave pattern, and a sawtooth wave pattern.
   @section sec_stadia  State Diagram
                        ![](Lab_01_State_Diagram.png)
   
   @section sec_LED     LED Show
                        This file contains the code that generates the three LED patterns.
    
   @section sec_video   LED Video
                        This is a link to watch the code in action with the LED. https://cpslo-my.sharepoint.com/personal/acarbaug_calpoly_edu/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Facarbaug%5Fcalpoly%5Fedu%2FDocuments%2FCurrent%20Courses%2FME%20305%20Lab%2FLab%200x01%2FIMG%5F4186%2EMOV&parent=%2Fpersonal%2Facarbaug%5Fcalpoly%5Fedu%2FDocuments%2FCurrent%20Courses%2FME%20305%20Lab%2FLab%200x01 

   @author              Carbaugh, Flaherty

   @date                October 5, 2021
'''
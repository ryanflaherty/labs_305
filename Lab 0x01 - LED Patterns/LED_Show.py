''' @file          LED_Show.py
    @brief         This file shows different LED patterns by clicking a button.
    @details       This file uses a button press to cycle a LED changing its 
                   brightness according to three different patters. The first 
                   pattern is a square wave, the second is a sine wave, and 
                   the third is a sawtooth wave.
    @author        Ayden Carbaugh
    @author        Ryan Flaherty
    @date          Oct 5, 2021
'''
# Import statements
import utime
import pyb
import math

def ButtonPress(IRQ_src):
    '''
    @brief     This function defines the button press as a interrupt sequence.
    @details   This function defines the button press as a global variable
               and an interrupt sequence to be used for state changes.
    @param     IRS_src sorurce of the interruption.
    @return    returns the button pressed as an ouput of 1.
    '''
    global button
    button = 1
    
def set_timer():
    '''
    @brief     This function sets the timer start time.
    @details   This function resets the timer and records it as the start 
               time to be used in later calculations.
    @param     This function uses the timer as its parameter.
    @return    This returns the start time of the wave pattern.
    '''
    starttime = utime.ticks_ms()
    return starttime

def update_timer():
    '''
    @brief     This function calculates the duration of the wave pattern.
    @details   This function updates the timer and calculates the duration of
               how long the wave pattern has been displayed.
    @param     This function uses the timer as its parameter.
    @return    This returns the duration of the wave pattern.
    '''
    stoptime = utime.ticks_ms()
    duration = utime.ticks_diff(stoptime,starttime)/1000 
    return duration

def update_SQW(duration):
    '''
    @brief     This calculates the brightness of the LED for the square wave.
    @details   This function calculates the brightness of the LED for the 
               square wave pattern as a function of time.
    @param     This function uses duration as its parameter.
    @return    This returns the the brightness of the LED as a decimal.
    '''
    return 100*(duration%1.0>0.5)

def update_STW(duration):
    '''
    @brief     This calculates the brightness of the LED for the sawtooth wave.
    @details   This function calculates the brightness of the LED for the 
               sawtooth wave pattern as a function of time.
    @param     This function uses duration as its parameter.
    @return    This returns the the brightness of the LED as a decimal.
    '''
    return 100*(duration%1.0)

def update_SW(duration):
    '''
    @brief     This calculates the brightness of the LED for the sine wave.
    @details   This function calculates the brightness of the LED for the 
               sine wave pattern as a function of time.
    @param     This function uses duration as its parameter.
    @return    This returns the the brightness of the LED as a decimal.
    '''
    return 0.5 + 0.5*math.sin(((math.pi)*duration/5))
    
#The main program should go after function definitions and run continuosly

if __name__== '__main__':
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=ButtonPress)
    ## The current state of the button
    button = 0
    ## The current state for this iteration of the FSM
    state = 0
    ## The number of iterations performed by the FSM
    runs = 0
    
    while(True):
        try:
            if (state==0):
                # Run state 0 code
                print('Hi, Please press the button "B1" to begin to cycle through LED patterns beginning at square wave.')
                state = 1                   # Transition to state 1
                
            elif (state==1):
                # Run state 1 code
                if (button == 1):
                    starttime=set_timer()
                    state = 2               # Transition to state 2
                    print('Currently viewing square pattern')
                    button = 0
                
            elif (state==2):
                # Run state 2 code
                if (button == 1):
                    starttime=set_timer()
                    print('Currently viewing sine pattern')
                    state = 3               # Transition to state 3
                    button = 0
                    continue
                else:
                    duration=update_timer()
                    t2ch1.pulse_width_percent(100*update_SQW(duration))
            elif (state==3):
                # Run state 1 code
                if (button == 1):
                    print('Currently viewing sawtooth pattern')
                    state = 4               # Transition to state 4
                    button = 0
                else:
                    duration=update_timer()
                    t2ch1.pulse_width_percent(100*update_SW(duration)) 
            elif (state==4):
                # Run state 1 code
                if (button == 1):
                    print('Currently viewing square pattern')
                    state = 2               # Transition to state 2
                    button = 0
                else:
                    duration=update_timer()
                    t2ch1.pulse_width_percent(update_STW(duration))
   
        except KeyboardInterrupt:           # If keyboard interrupt, exit loop
            break
        
    print('Program Terminating')
    
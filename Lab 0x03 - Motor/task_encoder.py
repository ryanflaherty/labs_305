'''
@file          task_encoder.py
@brief         This file creates the class for interacting with an encoder.
@details       This file creates a class for interacting with a quadrature 
               encoder. It does this through the use of an initialization, 
               update, get postiion, get delta, and set position functions.
               It completes this by initializing and running the encoder task.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
# Import statements
import utime
import encoder

class Task_Encoder:
    '''
    @brief   This class interacts with an encoder object.
    @details This class interacts with an encoder object through intialization
             and a run function to tak a user input and output the desired value.
    '''
    def __init__(self, period, position1_share, position2_share, delta1_share, delta2_share, enc_Flag):
        '''
        @brief     This function initializes the encoder task.
        @details   This function initializes the task by defining the period,
                   and creating e postion and delta to be shared with other tasks.
        @return    returns the variables intitialized.
        '''
        ## The position of encoder 1, created as a sared variable for other tasks.
        self.position1_share = position1_share
        ## The delta of encoder 1, created as a shared variable for other tasks.
        self.delta1_share = delta1_share
        ## The position of encoder 1, created as a sared variable for other tasks.
        self.position2_share = position2_share
        ## The delta of encoder 1, created as a shared variable for other tasks.
        self.delta2_share = delta2_share
        ## The flag variable that shares the current state of the finite state machine.
        self.enc_Flag = enc_Flag
        ## the number of runs of the code
        self.runs = 0
        ## The period 
        self.period = period
        ## calls the encoder function
        self.encoder = encoder.Encoder()
        ## sets the time elapsed for the next iteration
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)

    def run(self):
        '''
        @brief     This function runs the encoder task.
        @details   This function runs the task by taking a variety of inputs
                   and performing the tasks assigned to those inputs.
        @param     self.next_time
        @param     self.enc_Flag
        @return    This function returns the desired postion and delta.
        '''
        if utime.ticks_us() >= self.next_time:
            if self.enc_Flag.read() == 1:
                self.position1_share.write(self.encoder.set_position(0,1))
            elif self.enc_Flag.read() == 2:
                self.delta1_share.write(self.encoder.get_delta())
            elif self.enc_Flag.read() == 3:
                self.position2_share.write(self.encoder.set_position(0,2))
            elif self.enc_Flag.read() == 4:
                self.delta2_share.write(self.encoder.get_delta())
            else:
                self.encoder.update()
                self.position1_share.write(self.encoder.get_position())
                self.position2_share.write(self.encoder.get_position())
                self.delta1_share.write(self.encoder.get_delta())
                self.delta2_share.write(self.encoder.get_delta())
        
                self.next_time += self.period
                self.runs += 1
            
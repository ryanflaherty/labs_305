'''
@file          task_user.py
@brief         This file creates the class for interacting with the user.
@details       This file creates a class for interacting with a user to get the
               desired output for the input provided.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
# Import statements
import pyb
import utime
## Defines the state of the finite state machine as state 0
s0_init = 0
## Defines the state of the finite state machine as state 1
s1 = 1
## Defines the state of the finite state machine as state 2
s2 = 2
## Defines the state of the finite state machine as state 3
s3 = 3
##
s4 = 4
##
s5 = 5
##
s6 = 6
##
s7 = 7
class Task_User:
    '''@brief      This class interacts with an encoder task.
       @details    This class interacts with the user task to prompt a user
                   for their desired inpormation, then displays that
                   information after interacting with the encoder task.
    '''
    def __init__(self, period, position1_share, position2_share, delta1_share, delta2_share, enc_Flag):
        '''
        @brief     This function initializes the user task.
        @details   This function initializes the task by defining the state,
                   and creating an encoder variable.
        @param     s0_init the initial state.
        @return    returns the encoder variable.
        '''
        ## The postion of encoder 1, shared between multiple tasks
        self.position1_share = position1_share
        ## The postion of encoder 2, shared between multiple tasks
        self.position2_share = position2_share
        ## The delta of encoder 1, shared between multiple tasks
        self.delta1_share = delta1_share
        ## The delta of encoder 1, shared between multiple tasks
        self.delta2_share = delta2_share
        ## Sets the intiial state of the finite state machine as state 0
        self.state = s0_init
        ## The period of the task
        self.period = period
        ## Number of runs of the finite state machine
        self.runs = 0
        ## Serial port for user interfacing
        self.ser = pyb.USB_VCP()
        ## Time to run the next iteration of the task
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## A flag for the current state which is shared between all the tasks
        self.enc_Flag = enc_Flag
        ## Starts the enc flag at state 0
        self.enc_Flag.write(0)
        ## Variable used to index through lists
        self.idx = 0
        ## List used to display encoder 1 position while collecting data
        self.position_list1 = []
        ## List used to display encoder 1 position while collecting data
        self.position_list2 = []
        ## List used to display time while collecting data
        self.time_list1 = []
        ## List used to display time while collecting data
        self.time_list2 = []
        ## The period used to collect data during data collection
        self.time_period = 100
        ## The time to run the next iteration state 2
        self.next_time2= utime.ticks_add(utime.ticks_ms(),self.time_period)
        ## The reference time for beginning state 2
        self.Timref1 = 0
        ##
        self.Timref2
        ##
        self.my_str = ''
        
    def run(self):
        '''
        @brief     This function runs the user task.
        @details   This function runs the task by inputing instructions and
                   and prompting a key command input.
        @return    returns the desired output.
        '''
        ## User interface instructions
        self.instruct = """
                  "Instructions - press one of the following keys their respective functions: \n"
                  "z:   Zero the position of encoder 1\n"
                  "p:   Print the position of encoder 1\n"
                  "d:   Print the delta for encoder 1\n"
                  "g:   Collect data for encoder 1 for 30 seconds and print it as list\n"
                  "s:   End data collection prematurely\n"
                  "h:   Redisplay user command interface")
            """
        ## Current time of the iteration
        current_time = utime.ticks_us()    
        
        if utime.ticks_diff(current_time, self.next_time) >= 0:
            if self.state == s0_init:
                print(self.instruct)
                self.state = s1
            elif self.state == s1:
                self.enc_Flag.write(0)
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 'z':
                        self.enc_Flag.write(1)
                        print('Position of encoder 1 zeroed')
                    if self.char_in == 'Z':
                        self.enc_Flag.write(3)
                        print('Position of encoder 2 zeroed')
                    elif self.char_in == 'p':
                        print('Position of encoder 1 is {:}'.format(self.position1_share.read()))
                    elif self.char_in == 'P':
                        print('Position of encoder 2 is {:}'.format(self.position2_share.read()))
                    elif self.char_in == 'd':
                        self.enc_Flag.write(2)
                        print('Encoder 1 delta is {:}'.format(self.delta1_share.read()))
                    elif self.char_in == 'D':
                        self.enc_Flag.write(4)
                        print('Encoder 2 delta is {:}'.format(self.delta2_share.read()))
                    elif self.char_in == 'm':
                        self.state = s4
                        print('Please input a duty cycle for Motor 1')
                    elif self.char_in == 'M':
                        self.state = s5
                        print('Please input a duty cycle for Motor 2')
                    elif self.char_in == 'c':
                        self.mot_Flag.write(3)
                        print('Fault cleared')
                    elif self.char_in =='g':
                        self.state = s2
                        self.Timref1 = utime.ticks_ms()
                        self.end_30s1 = utime.ticks_add(self.Timref1, 30000)
                        print('Collecting data for encoder 1')
                    elif self.char_in =='G':
                        self.state = s6
                        self.Timref2 = utime.ticks_ms()
                        self.end_30s2 = utime.ticks_add(self.Timref2, 30000)
                        print('Collecting data for encoder 1')
                    elif self.char_in == 'h':
                        print(self.instruct)
                        pass
            
            elif self.state == s2:
                self.enc_Flag.write(0)
                self.idx = 0
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 's':
                        self.state = s3
                if utime.ticks_ms() >= self.end_30s1:
                    self.state = s3
                if utime.ticks_ms() >= self.next_time2:
                    self.position1_share.read()
                    self.position_list1.append(self.position1_share.read())
                    self.time_list1.append(utime.ticks_diff(utime.ticks_ms(), self.Timref1))
                    self.next_time2 = utime.ticks_add(utime.ticks_ms(), self.time_period)
            
            elif self.state == s3:
                for idx in range (len(self.position_list1)):
                    print('[{:},{:}]'.format(self.position_list1[idx],self.time_list1[idx]/1000))
                self.state = s1
                self.position_list1 = []
                self.time_list1 = []
                
            elif self.state == s4:
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in.isdigit() == True:
                        self.my_str += self.char_in
                        self.ser.write(self.char_in)
                    if self.char_in == '-':
                        if self.my_str == '':
                            self.my_str += self.char_in
                            self.ser.write(self.char_in)
                    elif self.char_in == '\x7F':
                        if self.my_str == '':
                            self.my_str = self.my_str
                            self.ser.write(self.char_in)
                        else:
                            a = len(self.my_str)
                            b = a-1
                            self.my_str = self.my_str[:b]
                            self.ser.write(self.char_in)
                    elif self.char_in == '\r' or self.char_in =='\n':
                        self.duty_share.write(int(self.my_str))
                        self.mot_Flag.write(1)
                        self.state = s1
                        self.my_str = ''
            elif self.state == s5:
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in.isdigit() == True:
                        self.my_str += self.char_in
                        self.ser.write(self.char_in)
                        if self.char_in == '-':
                            if self.my_str == '':
                                self.my_str += self.char_in
                                self.ser.write(self.char_in)
                            elif self.char_in == '\x7F':
                                if self.my_str == '':
                                    self.my_str = self.my_str
                                    self.ser.write(self.char_in)
                                else:
                                    a = len(self.my_str)
                                    b = a-1
                                    self.my_str = self.my_str[:b]
                                    self.ser.write(self.char_in)
                            elif self.char_in == '\r' or self.char_in =='\n':
                                self.duty_share.write(int(self.my_str))
                                self.mot_Flag.write(2)
                                self.state = s1
                                self.my_str = ''
            elif self.state == s6:
                self.enc_Flag.write(0)
                self.idx = 0
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 's':
                        self.state = s7
                if utime.ticks_ms() >= self.end_30s2:
                    self.state = s3
                if utime.ticks_ms() >= self.next_time2:
                    self.position1_share.read()
                    self.position_list2.append(self.position1_share.read())
                    self.time_list2.append(utime.ticks_diff(utime.ticks_ms(), self.Timref2))
                    self.next_time2 = utime.ticks_add(utime.ticks_ms(), self.time_period)   
            elif self.state == s7:
                for idx in range (len(self.position_list2)):
                    print('[{:},{:}]'.format(self.position_list2[idx],self.time_list2[idx]/1000))
                self.state = s1
                self.position_list2 = []
                self.time_list2 = []
           
            else:
                raise ValueError('Invalid state')
                
            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1
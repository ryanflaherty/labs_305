'''@file                mainpage.py
   @brief               This file creates the main page for the project.
   @details             This file contains the text to generate the proper 
                        structure for the main page of the project. 

   @mainpage

   @section sec_intro   Introduction
                        This project creates code to interact with a user to 
                        spin two motors and use encoders to display the encoder
                        position, delta, or take data of encoder's position
                        over a period of time.
                        
   @section sec_stadia  Block Diagram
                        This image is the block diagram of the closed loop
                        control system for Lab 0x04.
                        ![](Block_Diagram.png)
                        
   @section main        Main Script
                        This file contains the main script which calls and runs
                        the encoder, motor,and user task.
   
   @section user_task   User Task
                        This script creates the task for the user interface, 
                        where a used can input a keyboard command and will
                        receive the wanted data values. 
                        
   @section enc_task    Encoder Task
                        This script creates the task for interacting with an
                        encoder object. This task will obtain data from the 
                        encoder object and will relay it to the user task.
  
   @section mot_task    Motor Task
                        This script creates the task for interacting with a 
                        motor object. This task will obtain data from the user
                        task and will use it to spin the motors.
        
   @section enc_class   Encoder Class
                        This script creates the object for interfacing with a
                        quadrature encoder. The functions in this class can be
                        called by tasks to display the desired information.
                        
   @section mot_class   Motor Class
                        This script creates the object for interfacing with two
                        PMDC motor. The functions in this class can be called
                        by tasks to send or obtain the desired information.
   
   @section clos_task   Closed Loop Controller 
                        This script creates the controller that compares the
                        measured velocity of motor 1 and motor 2 to the 
                        reference velocity given by the user. It then applies 
                        a proportional gain value, specified by the user, to 
                        the velocity to adjust it. With a reference velocity of 
                        25 rad/s our group applied a gain value of 1,2,3,4, 
                        and 5. It was found that values under 2 did not produce
                        noticable data change and a gain of 5 caused the motor 
                        to begin to oscillate, making 3 or 4 an ideal 
                        proportional gain.
                        
   @section share_class Shares Class
                        This script creates shared variables for tasks to 
                        interact with each other.
                        
   @section sec_code    Source Code Link
                        This link will direct you towards the file containing 
                        the source code for this project. https://bitbucket.org/acarbaugh/me_305_ayden/src/master/Lab%200x03%20and%200x04%20-%20Closed%20Loop%20Control/
  
   @author              Carbaugh, Flaherty

   @date                October 18, 2021
'''
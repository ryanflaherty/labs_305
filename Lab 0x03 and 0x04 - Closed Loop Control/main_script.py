'''
@file          main_script.py
@brief         This file calls the tasks required.
@details       This file calls the encoder task and the user interface task.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
#Import statements
import task_encoder
import task_user
import task_motor
import encoder
import shares

def main():
    '''
    @brief   This script runs the user and encoder tasks
    @details This script initiializes the encoder and runs task_user, 
             task_encoder and task_motor.
    '''
    ## This variable creates a position which is shared between all the tasks
    position1_share = shares.Share(0)
    ## This variable creates a position which is shared between all the tasks
    position2_share = shares.Share(0)
    ## This variable creates a delta which is shared between all the tasks
    delta1_share = shares.Share(0)
    ## This variable creates a delta which is shared between all the tasks
    delta2_share = shares.Share(0)
    ## This variable creates a duty cycle which is shared between all the tasks
    duty_share = shares.Share(0)
    ## This variable creates an omega which is shared between all the tasks
    omega_share = shares.Share(0)
    ## This variable creates a measured omega for motor 1 which is shared between all the tasks
    omega1_share = shares.Share(0)
    ## This variable creates a measured omega for motor 2 which is shared between all the tasks
    omega2_share = shares.Share(0)
    ## This variable creates a gain value which is shared between all the tasks
    gain_share = shares.Share(0)
    ## This variable creates a flag for the current state which is shared between all the tasks
    enc_Flag = shares.Share(0)
    ## This variable creates a flag for the current state which is shared between all the tasks
    mot_Flag = shares.Share(0)
    ## This intializes the encoder
    encoder.Encoder()
    ## Defines task 1 as the encoder task
    task1 = task_encoder.Task_Encoder(5_000, position1_share, position2_share, delta1_share, delta2_share, omega1_share, omega2_share, enc_Flag)
    ## Defines task 2 as the user task
    task2 = task_user.Task_User(5_000, position1_share, position2_share, delta1_share, delta2_share, omega_share, omega1_share, omega2_share, duty_share, gain_share, enc_Flag, mot_Flag)
    ## Defines task 3 as the motor task
    task3 = task_motor.Task_Motor(5_000, duty_share, omega_share, omega1_share, omega2_share, gain_share, mot_Flag)
    ## Creates a list to run both tasks while in a while loop
    task_list = [task1, task2, task3]

    while(True):
       try:
          for task in task_list:
              task.run() 
       except KeyboardInterrupt:
           break
           print('Program Terminating')

if __name__ == '__main__':
    main()
            
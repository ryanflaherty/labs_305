'''
@file          closedloop.py
@brief         This file creates the class for creating closed loop control.
@details       This file creates the class for creating a closed loop 
               controller using a proportional gain.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
class ClosedLoop:
    ''' 
    @brief     This class creates a task to implement closed loop control.
    @details   This class creates a task to implement closed loop control using
               a proportional input value.
    '''    
    def __init__(self, gain_share, max_lim, min_lim):
        '''
        @brief     This function initializes the closedloop driver.
        @details   This function initializes the closedloop  driver by creating
                   measured and referenced omega values.
        '''
        self.gain_share = gain_share
        self.max_lim = max_lim
        self.min_lim = min_lim
        self.omea_ref = 0
        self.omega_meas = 0
        
    def update(self, omega_ref, omega_meas):
        '''
        @brief     This function runs the closedloop task.
        @details   This function runs the closedloop task by taking referenced
                   and measured velocity data and modifying that actuation of
                   the motor.
        @return    self.actuation
        '''
        self.max_lim = 100
        self.min_lim = -100
        self.omega_ref = omega_ref
        self.omega_meas = omega_meas
        self.actuation = self.gain_share*(self.omega_ref - self.omega_meas)
        if self.actuation >= self.max_lim:
            self.actuation = self.max_lim
        elif self.actuation <= self.min_lim:
            self.actuation = self.min_lim
        return self.actuation
        
    def get_kp(self,gain_share):
        '''
        @brief     This function returns the gain value.
        @details   This function returns the gain value to be used by tasks.
        @return    gain_share
        '''
        return self.gain_share
    
    def set_kp(self, gain_share):
        '''
        @brief     This function sets the gain value.
        @details   This functionsets the gain value to be used by tasks.
        '''
        self.gain_share = gain_share
        
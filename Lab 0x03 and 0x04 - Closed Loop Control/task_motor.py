'''
@file          task_motor.py
@brief         This file creates the class for interacting with the motor.
@details       This file creates a class for interacting with a motor to get 
               the desired output for the input provided by the user task.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
import DRV8847
import utime
import closedloop

class Task_Motor:
    ''' 
    @brief     This class creates a task for interfacing with a motor object.
    @details   This class creates a task for interfacing with a motor objects
               using shares values from other scripts.
    '''    
    def __init__(self, period, duty_share, omega_share, omega1_share, omega2_share, gain_share, mot_Flag):
        '''
        @brief     This function initializes the motor task.
        @details   This function initializes the task by impolementing shared 
                   variables.
        '''
        ## A shared motor flag variable
        self.mot_Flag = mot_Flag
        ## The period
        self.period = period
        ## Measured speed value for motor 1
        self.omega1_share = omega1_share
        ## Measured speed value for motor 2
        self.omega2_share = omega2_share
        ## The speed desired
        self.omega_share = omega_share
        ## The duty cycle for the motors
        self.duty_share = duty_share
        ## The gain value for modifying the speed
        self.gain_share = gain_share
        ## The time for next iteration
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## The number of runs through the code
        self.runs = 0
        ## Initializes the motor object
        self.motor_drv = DRV8847.DRV8847()
        ## Creates the object for motor 1
        self.motor_1 = self.motor_drv.motor(1)
        ## Creates the object for motor 2
        self.motor_2 = self.motor_drv.motor(2)
        ## Initialized the P controller
        self.closedloop = closedloop.Closed_Loop(1, 100, -100)
        
    def run(self):
        if utime.ticks_us() >= self.next_time:
            if self.mot_Flag.read() == 1:
                self.motor_drv.enable()
                self.motor_1.set_duty(self.duty_share.read())
                self.mot_Flag.write(0)
                
            elif self.mot_Flag.read() == 2:
                self.motor_drv.enable()
                self.motor_2.set_duty(self.duty_share.read())
                self.motor_flag.write(0)
                
            elif self.mot_Flag.read() == 3:
                self.motor_drv.enable()
            
            elif self.mot_Flag.read() == 4:
                self.duty1 = self.closedloop.update(self.omega_share.read, self.omega1_share.read())
                self.motor_1.set_duty(self.duty1)
                self.duty_share.write(self.duty1)
            
            elif self.mot_Flag.read() == 5:
                self.duty2 = self.closedloop.update(self.omega_share.read(), self.omega2_share.read())
                self.motor_2.set_duty(self.duty2)
                self.duty_share.write(self.duty2)
            elif self.mot_Flag.read() == 6:
                self.closedloop.set_kp(self.gain_share.read())
                
            else:
                self.next_time += self.period
                self.runs +=1
'''
@file          task_user.py
@brief         This file creates the class for interacting with the user.
@details       This file creates a class for interacting with a user to get the
               desired output for the input provided.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
# Import statements
import pyb
import utime
## Defines the state of the finite state machine as state 0
s0_init = 0
## Defines the state of the finite state machine as state 1
s1 = 1
## Defines the state of the finite state machine as state 2
s2 = 2
## Defines the state of the finite state machine as state 3
s3 = 3
## Defines the state of the finite state machine as state 4
s4 = 4
## Defines the state of the finite state machine as state 5
s5 = 5
## Defines the state of the finite state machine as state 6
s6 = 6
## Defines the state of the finite state machine as state 7
s7 = 7
## Defines the state of the finite state machine as state 8
s8 = 8
## Defines the state of the finite state machine as state 9
s9 = 9
## Defines the state of the finite state machine as state 10
s10 = 10
## Defines the state of the finite state machine as state 11
s11 = 11
## Defines the state of the finite state machine as state 12
s12 = 12
## Defines the state of the finite state machine as state 13
s13 = 13
## Defines the state of the finite state machine as state 14
s14 = 14
## Defines the state of the finite state machine as state 15
s15 = 15

class Task_User:
    '''@brief      This class interacts with an encoder task.
       @details    This class interacts with the user task to prompt a user
                   for their desired inpormation, then displays that
                   information after interacting with the encoder task.
    '''
    def __init__(self, period, position1_share, position2_share, delta1_share, delta2_share, omega_share, omega1_share, omega2_share, duty_share, gain_share, enc_Flag, mot_Flag):
        '''
        @brief     This function initializes the user task.
        @details   This function initializes the task by defining the state,
        '''
        ## The postion of encoder 1, shared between multiple tasks
        self.position1_share = position1_share
        ## The postion of encoder 2, shared between multiple tasks
        self.position2_share = position2_share
        ## The delta of encoder 1, shared between multiple tasks
        self.delta1_share = delta1_share
        ## The delta of encoder 2, shared between multiple tasks
        self.delta2_share = delta2_share
        ## The duty cycle for the motor
        self.duty_share = duty_share
        ## The angular speed of the encoder
        self.omega_share = omega_share
        ## The measured velocity if motor 1
        self.omega1_share = omega1_share
        ## The measured velocity if motor 2
        self.omega2_share = omega2_share
        ## The proportional gain for the controller
        self.gain_share = gain_share
        ## Sets the intiial state of the finite state machine as state 0
        self.state = s0_init
        ## The period of the task
        self.period = period
        ## Number of runs of the finite state machine
        self.runs = 0
        ## Serial port for user interfacing
        self.ser = pyb.USB_VCP()
        ## Time to run the next iteration of the task
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## A flag for the current state which is shared between all the tasks
        self.enc_Flag = enc_Flag
        ## Starts the enc flag at state 0
        self.enc_Flag.write(0)
        ## A flag fotr the current state of the motor
        self.mot_Flag = mot_Flag
        ## Variable used to index through lists
        self.idx = 0
        ## List used to display encoder 1 position while collecting data
        self.position_list = []
        ## List used to display time while collecting data
        self.time_list = []
        ## List 
        self.actuation_list = []
        ## The period used to collect data during data collection
        self.time_period = 100
        ## The time to run the next iteration state 2
        self.next_time2= utime.ticks_add(utime.ticks_ms(),self.time_period)
        ## The reference time for beginning state 2
        self.Timref = 0
        ## A blank string for character input
        self.my_str = ''
        
    def run(self):
        '''
        @brief     This function runs the user task.
        @details   This function runs the task by inputing instructions and
                   and prompting a key command input.
        @return    returns the desired output.
        '''
        ## User interface instructions
        self.instruct = """
                  -------------------------------------------------------------------------------         
                  Instructions - press one of the following keys their respective functions:
                  ------------------------------------------------------------------------------- 
                  z: Zero the position of encoder 1
                  Z: Zero the position of encoder 2
                  p: Print the position of encoder 1
                  P: Print the position of encoder 2
                  d: Print the delta for encoder 1
                  D: Print the delta for encoder 2
                  m: Enter the duty cycle for motor 1
                  M: Enter the duty cycle for motor 2
                  c: Clear the fault condition for the motors
                  g: Collect data for encoder 1 for 30 seconds and print it as list
                  G: Collect data for encoder 2 for 30 seconds and print it as list
                  s: End data collection for encoder 1 prematurely
                  S: End data collection for encoder 2 prematurely
                  h: Redisplay user command interface
                  -------------------------------------------------------------------------------
                  1: Perform a step response for motor 1
                  2: Perform a step response for motor 2
                  s: End data collection prematurely
            """
        ## Current time of the iteration
        current_time = utime.ticks_us()    
        
        if utime.ticks_diff(current_time, self.next_time) >= 0:
            if self.state == s0_init:
                print(self.instruct)
                self.state = s1
            elif self.state == s1:
                self.enc_Flag.write(0)
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 'z':
                        self.enc_Flag.write(1)
                        print('Position of encoder 1 zeroed')
                    if self.char_in == 'Z':
                        self.enc_Flag.write(3)
                        print('Position of encoder 2 zeroed')
                    elif self.char_in == 'p':
                        self.enc_Flag.write(5)                    
                    elif self.char_in == 'P':
                        self.enc_Flag.write(6)                    
                    elif self.char_in == 'd':
                        self.enc_Flag.write(2)
                    elif self.char_in == 'D':
                        self.enc_Flag.write(4)
                    elif self.char_in == 'm':
                        self.state = s4
                        print('Please input a duty cycle for Motor 1')
                    elif self.char_in == 'M':
                        self.state = s5
                        print('Please input a duty cycle for Motor 2')
                    elif self.char_in == 'c':
                        self.mot_Flag.write(3)
                        print('Fault cleared')
                    elif self.char_in =='g':
                        self.state = s2
                        self.Timref = utime.ticks_ms()
                        self.end_30s = utime.ticks_add(self.Timref, 30000)
                        print('Collecting data for encoder 1')
                    elif self.char_in =='G':
                        self.state = s6
                        self.Timref = utime.ticks_ms()
                        self.end_30s = utime.ticks_add(self.Timref, 30000)
                        print('Collecting data for encoder 2')
                    elif self.char_in == '1':
                        self.state = s8
                        print('Please enter a proprtional gain value')
                    elif self.char_in == '2':
                        self.state = s12
                    elif self.char_in == 'h':
                        print(self.instruct)
                    else:
                        print('Please input a valid command')
                        pass
            
            elif self.state == s2:
                self.enc_Flag.write(0)
                self.idx = 0
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 's':
                        self.state = s3
                if utime.ticks_ms() >= self.end_30s:
                    self.state = s3
                if utime.ticks_ms() >= self.next_time2:
                    self.position1_share.read()
                    self.position_list.append(self.position1_share.read())
                    self.time_list.append(utime.ticks_diff(utime.ticks_ms(), self.Timref))
                    self.next_time2 = utime.ticks_add(utime.ticks_ms(), self.time_period)
            
            elif self.state == s3:
                for idx in range (len(self.position_list)):
                    print('[{:},{:}]'.format(self.position_list[idx],self.time_list[idx]/1000))
                self.state = s1
                self.position_list = []
                self.time_list = []
                
            elif self.state == s4:
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in.isdigit() == True:
                        self.my_str += self.char_in
                        self.ser.write(self.char_in)
                    if self.char_in == '-':
                        if self.my_str == '':
                            self.my_str += self.char_in
                            self.ser.write(self.char_in)
                    elif self.char_in == '\x7F':
                        if self.my_str == '':
                            self.my_str = self.my_str
                            self.ser.write(self.char_in)
                        else:
                            a = len(self.my_str)
                            b = a-1
                            self.my_str = self.my_str[:b]
                            self.ser.write(self.char_in)
                    elif self.char_in == '\r' or self.char_in =='\n':
                        self.duty_share.write(int(self.my_str))
                        self.mot_Flag.write(1)
                        self.state = s1
                        self.my_str = ''
            elif self.state == s5:
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in.isdigit() == True:
                        self.my_str += self.char_in
                        self.ser.write(self.char_in)
                        if self.char_in == '-':
                            if self.my_str == '':
                                self.my_str += self.char_in
                                self.ser.write(self.char_in)
                            elif self.char_in == '\x7F':
                                if self.my_str == '':
                                    self.my_str = self.my_str
                                    self.ser.write(self.char_in)
                                else:
                                    a = len(self.my_str)
                                    b = a-1
                                    self.my_str = self.my_str[:b]
                                    self.ser.write(self.char_in)
                            elif self.char_in == '\r' or self.char_in =='\n':
                                self.duty_share.write(int(self.my_str))
                                self.mot_Flag.write(2)
                                self.state = s1
                                self.my_str = ''
            elif self.state == s6:
                self.enc_Flag.write(0)
                self.idx = 0
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 'S':
                        self.state = s7
                if utime.ticks_ms() >= self.end_30s:
                    self.state = s7
                if utime.ticks_ms() >= self.next_time2:
                    self.position2_share.read()
                    self.position_list.append(self.position2_share.read())
                    self.time_list2.append(utime.ticks_diff(utime.ticks_ms(), self.Timref))
                    self.next_time2 = utime.ticks_add(utime.ticks_ms(), self.time_period)   
            elif self.state == s7:
                for idx in range (len(self.position_list)):
                    print('[{:},{:}]'.format(self.position_list[idx],self.time_list[idx]/1000))
                self.state = s1
                self.position_list = []
                self.time_list = []
            elif self.state == s8:
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in.isdigit() == True:
                        self.my_str += self.char_in
                        self.ser.write(self.char_in)
                        if self.char_in == '-':
                            if self.my_str == '':
                                self.my_str += self.char_in
                                self.ser.write(self.char_in)
                            elif self.char_in == '\x7F':
                                if self.my_str == '':
                                    self.my_str = self.my_str
                                    self.ser.write(self.char_in)
                                else:
                                    a = len(self.my_str)
                                    b = a-1
                                    self.my_str = self.my_str[:b]
                                    self.ser.write(self.char_in)
                            elif self.char_in == '\r' or self.char_in =='\n':
                                self.duty_share.write(int(self.my_str))
                                self.mot_Flag.write(6)
                                self.state = s9
                                print('Please enter a desired velocity setpoint')
                                self.my_str = ''
            
            elif self.state == s9:
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in.isdigit() == True:
                        self.my_str += self.char_in
                        self.ser.write(self.char_in)
                        if self.char_in == '-':
                            if self.my_str == '':
                                self.my_str += self.char_in
                                self.ser.write(self.char_in)
                            elif self.char_in == '\x7F':
                                if self.my_str == '':
                                    self.my_str = self.my_str
                                    self.ser.write(self.char_in)
                                else:
                                    a = len(self.my_str)
                                    b = a-1
                                    self.my_str = self.my_str[:b]
                                    self.ser.write(self.char_in)
                            elif self.char_in == '\r' or self.char_in =='\n':
                                self.duty_share.write(int(self.my_str))
                                self.mot_Flag.write(0)
                                self.state = s10
                                print('Please enter a desired velocity setpoint')
                                self.my_str = ''
                                self.Timref = utime.ticks_ms()
                                self.end_10s = utime.ticks_add(self.Timref, 10000)
                                self.end_sta = utime.ticks_Add(self.Timref, 10)
                                self.mot_Flad.write(4)
            elif self.state == s10:
                self.id = 0
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 's':
                        self.state = s11
                        self.duty_share.write(0)
                        self.mot_Flag.write(1)
                        self.omega_share.write(0)
                if utime.ticks_ms() >= self.end_sta:
                    if self.delta1_share.read() == 0:
                        self.state = s11
                        self.duty_share.write(0)
                        self.mot_Flag.write(1)
                        self.omega_share.write(0)
                        print('Fault detected')
                if utime.ticks_ms() >= self.end_10s:
                    self.state = s11
                    self.duty_share.write(0)
                    self.mot_Flag.write(1)
                    self.omega_share.write(0)
                if utime.ticks_ms() >= self.next_time2:
                    self.position_list.append(self.omega1_share.read())
                    self.time_list.append(utime.ticks_diff(utime.ticks_ms(), self.Timref))
                    self.actuation_list.append(self.duty_share.read())
                    self.next_time2 = utime.ticks_add(utime.ticks_ms(), self.time_period)
            elif self.state == s11:
                for idx in range(len(self.position_list)):
                    print('{:}, {:}, {:}'.format(self.position_list[idx], self.time_list[idx]/1000, self.actuation_list[idx]))
                self.state = s1
                self.position_list = []
                self.time_list = []
                self.actuation_list = []
                self.duty_share.write(1)
                self.mot_Flag.write(1)
            elif self.state == s12:
                print('Please enter a proportional gain value')
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in.isdigit() == True:
                        self.my_str += self.char_in
                        self.ser.write(self.char_in)
                        if self.char_in == '-':
                            if self.my_str == '':
                                self.my_str += self.char_in
                                self.ser.write(self.char_in)
                            elif self.char_in == '\x7F':
                                if self.my_str == '':
                                    self.my_str = self.my_str
                                    self.ser.write(self.char_in)
                                else:
                                    a = len(self.my_str)
                                    b = a-1
                                    self.my_str = self.my_str[:b]
                                    self.ser.write(self.char_in)
                            elif self.char_in == '\r' or self.char_in =='\n':
                                self.duty_share.write(int(self.my_str))
                                self.mot_Flag.write(6)
                                self.state = s13
                                self.my_str = ''
            elif self.state == s13:
                print('enter a velocity setpoint')
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in.isdigit() == True:
                        self.my_str += self.char_in
                        self.ser.write(self.char_in)
                        if self.char_in == '-':
                            if self.my_str == '':
                                self.my_str += self.char_in
                                self.ser.write(self.char_in)
                            elif self.char_in == '\x7F':
                                if self.my_str == '':
                                    self.my_str = self.my_str
                                    self.ser.write(self.char_in)
                                else:
                                    a = len(self.my_str)
                                    b = a-1
                                    self.my_str = self.my_str[:b]
                                    self.ser.write(self.char_in)
                            elif self.char_in == '\r' or self.char_in =='\n':
                                self.omega_share.write(int(self.my_str))
                                self.enc_Flag.write(0)
                                self.state = s14
                                self.my_str = ''
                                self.Timref = utime.ticks_ms()
                                self.end_10s = utime.ticks_ass(self.Timref, 10000)
                                self.end_sta = utime.ticks_add(self.Timref, 10)
                                print('Running motor 2 for 10 seconds')
                                self.mot_Flag.write(5)
            elif self.state == s14:
                self.idx = 0
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 's' or self.char_in == 'S':
                        self.state = s15
                        self.duty_share.write(0)
                        self.mot_Flag.write(1)
                        self.omega_share.write(0)
                if (utime.ticks_ms() >= self.end_sta):
                    if self.delta2_share.read() == 0:
                        self.state = s15
                        self.duty_share.write(0)
                        self.mot_Flag.write(1)
                        self.omega_share.write(0)
                        print('Fault detected, program stopped')
                if (utime.ticks_ms() >= self.end_10):
                    self.state = s15
                    self.duty_share.write(0)
                    self.mot_Flag.write(1)
                    self.omega_share.write(0)
                if(utime.ticks_ms() >= self.next_time2):
                    self.delta2_share.read()
                    self.position_list.append(self.omega2_share.read())
                    self.time_list.append(utime.ticks_diff(utime.ticks_ms(),self.Timref))
                    self.actuation_list.append(self.duty_share.read())
                    self.next_time2= utime.ticks_add(utime.ticks_ms(),self.time_period)   
            elif self.state == s15:
                for idx in range(len(self.my_list_pos)):
                    print('{:} {:} {:}'.format(self.position_list[idx],self.time_list[idx]/1000,self.actuation_list[idx]))
                self.state = s1
                self.position_list = []
                self.time_list = []  
                self.actuation_list = []
            else:
                raise ValueError('Invalid state')
                
            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1
'''
@file          main_script.py
@brief         This file calls the tasks required.
@details       This file calls the encoder task and the user interface task.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''

import pyb
import utime


class Panel:
    '''
    @brief 
    @details
    '''
    def __init__(self):
        self.Pinxm = pyb.Pin(pyb.Pin.cpu.A1)
        self.Pinym = pyb.Pin(pyb.Pin.cpu.A0)
        self.Pinxp = pyb.Pin(pyb.Pin.cpu.A7)
        self.Pinyp = pyb.Pin(pyb.Pin.cpu.A6)
        
    def xscan(self):
        self.Pinxm = pyb.Pin(self.Pinxm, pyb.Pin.OUT_PP)
        self.Pinym = pyb.Pin(self.Pinym, pyb.Pin.IN)
        self.Pinxp = pyb.Pin(self.Pinxp, pyb.Pin.OUT_PP)
        self.Pinyp = pyb.Pin(self.Pinyp, pyb.Pin.IN)
        self.Pinxm.low()
        self.Pinxp.high()
        utime.sleep_us(4)
        self.adcx = pyb.ADC(self.Pinym)
        self.valx = self.adcx.read()
        return self.valx  
    
    def yscan(self):
        self.Pinxm = pyb.Pin(self.Pinxm, pyb.Pin.IN)
        self.Pinym = pyb.Pin(self.Pinym, pyb.Pin.OUT_PP)
        self.Pinxp = pyb.Pin(self.Pinxp, pyb.Pin.IN)
        self.Pinyp = pyb.Pin(self.Pinyp, pyb.Pin.OUT_PP)
        self.Pinym.low()
        self.Pinyp.high()
        utime.sleep_us(4)
        self.adcy = pyb.ADC(self.Pinxm)
        self.valy = self.adcy.read()
        return self.valy
    
    def zscan(self):
        self.Pinxm = pyb.Pin(self.Pinxm, pyb.Pin.OUT_PP)
        self.Pinym = pyb.Pin(self.Pinym, pyb.Pin.IN)
        self.Pinxp = pyb.Pin(self.Pinxp, pyb.Pin.IN)
        self.Pinyp = pyb.Pin(self.Pinyp, pyb.Pin.OUT_PP)
        self.Pinxm.low()
        self.Pinyp.high()
        utime.sleep_us(4)
        self.adcz = pyb.ADC(self.Pinxp)
        self.valz = self.adcz.read()
        if self.valz < 4000:
            self.valz = True
        else:
            self.valz = False
        return self.valz
    
    def scan(self):
        self.scan_tup = (self.valx, self.valy, self.valz)
        return self.scan_tup
        
        
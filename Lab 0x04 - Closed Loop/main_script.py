'''
@file          main_script.py
@brief         This file calls the tasks required.
@details       This file calls the encoder task and the user interface task.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
#Import statements
import task_encoder
import task_user
import task_motor
import DRV8847
import encoder
import shares

def main():
    '''
    @brief   This script runs the user and encoder tasks
    @details This script initiializes the encoder and runs task_user and task_encoder 
    '''
    ## This variable creates a position which is shared between all the tasks
    position1_share = shares.Share(0)
    ## This variable creates a position which is shared between all the tasks
    position2_share = shares.Share(0)
    ## This variable creates a delta which is shared between all the tasks
    delta1_share = shares.Share(0)
    ## This variable creates a delta which is shared between all the tasks
    delta2_share = shares.Share(0)
    ##
    duty_share = shares.Share(0)
    ## This variable creates a flag for the current state which is shared between all the tasks
    enc_Flag = shares.Share(0)
    ## This intializes the encoder
    mot_Flag = shares.Share(0)
    encoder.Encoder()
    ## Defines task 1 as the encoder task
    task1 = task_encoder.Task_Encoder(5_000, position1_share, position2_share, delta1_share, delta2_share, enc_Flag)
    ## Defines task 2 as the user task
    task2 = task_user.Task_User(5_000, position1_share, position2_share, delta1_share, delta2_share, enc_Flag)
    ## Defines task 3 as the motor task
    task3 = task_motor.Task_Motor(5_000, mot_Flag, duty_share)
    ## Creates a list to run both tasks while in a while loop
    task_list = [task1, task2, task3]

    while(True):
       try:
          for task in task_list:
              task.run() 
       except KeyboardInterrupt:
           break
           print('Program Terminating')

if __name__ == '__main__':
    main()
            
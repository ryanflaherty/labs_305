import closedloop
import utime

class Task_Controller:
    
    def __init__(self, period, delta, omega_ref, kp_share, duty_share):
        self.delta_share = delta
        self.omega_ref = omega_ref
        self.kp_share = kp_share
        self.closedloop = closedloop.ClosedLoop(self.delta_share, self.omega_ref, self.kp_share)
        self.point_value = self.closedloop
        self.duty_share = duty_share
        self.period = period
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        self.time_col = utime.ticks_add(utime.ticks_us(), self.period)
        
    def run(self):
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            self.point_value = self.closedloop.run()
            return self.closedloop.run()
        else:
            return self.point_value
        
        self.next_time = utime.ticks_add(self.next_time, self.period)
        
        
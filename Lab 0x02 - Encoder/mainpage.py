'''@file                mainpage.py
   @brief               This file creates the main page for the project.
   @details             This file contains the text to generate the proper 
                        structure for the main page of the project. 

   @mainpage

   @section sec_intro   Introduction
                        This project creates code to interact with a user and 
                        an encoder to display the encoder position, delta, or 
                        take data of encoder's position over a period of time.
                        
   @section sec_stadia  State and Task Diagrams
                        This image shows the state diagram for the user 
                        task for lab 0x02.
                        ![](Lab_02_State_Diagram_user.png)
                        
                        This image shows the state diagram for the 
                        encoder task for lab 0x02.
                        ![](Lab_02_State_Diagram_encoder.png)
                        
                        This image shows the task diagram for the encoder task
                        to interact with the user task.
                        ![](Lab_02_Task_Diagram.png)
                        
                        
   @section main        Main Script
                        This file contains the main script which calls and runs
                        the encoder and user task.
   
   @section user_task   User Task
                        This script creates the task for the user interface, 
                        where a used can input a keyboard command and will
                        receive the wanted data values. 
                        
   @section enc_task    Encoder Task
                        This script creates the task for interacting with an
                        encoder object. This task will obtain data from the 
                        encoder object and will relay it to the user task.
        
   @section enc_class   Encoder Class
                        This script creates the object for interfacing with a
                        quadrature encoder. This functions in this class can be
                        called by tasks to display the desired information.
                        
   @section share_class Shares Class
                        This script creates shared variables for tasks to 
                        interact with each other.
                        
   @section sec_code    Source Code Link
                        This link will direct you towards the file containing 
                        the source code for this project. https://bitbucket.org/acarbaugh/me_305_ayden/src/master/Lab%200x02%20-%20Encoder/
  
   @author              Carbaugh, Flaherty

   @date                October 18, 2021
'''
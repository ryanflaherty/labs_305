'''
@file          main_script.py
@brief         This file calls the tasks required.
@details       This file calls the encoder task and the user interface task.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
#Import statements
import task_encoder
import task_user
import encoder1
import shares

def main():
    '''
    @brief   This script runs the user and encoder tasks
    @details This script initiializes the encoder and runs task_user and task_encoder 
    '''
    ## This variable creates a position which is shared between all the tasks
    position_share = shares.Share(0)
    ## This variable creates a delta which is shared between all the tasks
    delta_share = shares.Share(0)
    ## This variable creates a flag for the current state which is shared between all the tasks
    enc_Flag = shares.Share(0)
    ## This intializes the encoder
    encoder1.Encoder()
    ## Defines task 1 as the encoder task
    task1 = task_encoder.Task_Encoder(5_000, position_share, delta_share, enc_Flag)
    ## Defines task 2 as thew user task
    task2 = task_user.Task_User(5_000, position_share, delta_share, enc_Flag)
    ## Creates a list to run both tasks while in a while loop
    task_list = [task1, task2]

    while(True):
       try:
          for task in task_list:
              task.run() 
       except KeyboardInterrupt:
           break
           print('Program Terminating')

if __name__ == '__main__':
    main()
            
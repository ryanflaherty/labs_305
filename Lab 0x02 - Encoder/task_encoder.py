'''
@file          task_encoder.py
@brief         This file creates the class for interacting with an encoder.
@details       This file creates a class for interacting with a quadrature 
               encoder. It does this through the use of an initialization, 
               update, get postiion, get delta, and set position functions.
               It completes this by initializing and running the encoder task.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
# Import statements
import utime
import encoder1

class Task_Encoder:
    '''
    @brief   This class interacts with an encoder object.
    @details This class interacts with an encoder object through intialization
             and a run function to tak a user input and output the desired value.
    '''
    def __init__(self, period, position_share, delta_share, enc_Flag):
        '''
        @brief     This function initializes the encoder task.
        @details   This function initializes the task by defining the period,
                   and creating e postion and delta to be shared with other tasks.
        @return    returns the variables intitialized.
        '''
        ## The position of encoder 1, created as a sared variable for other tasks.
        self.position_share = position_share
        ## The delta of encoder 1, created as a shared variable for other tasks.
        self.delta_share = delta_share
        ## The flag variable that shares the current state of the finite state machine.
        self.enc_Flag = enc_Flag
        ## the number of runs of the code
        self.runs = 0
        ## The period 
        self.period = period
        ## calls the encoder function
        self.encoder = encoder1.Encoder()
        ## sets the time elapsed for the next iteration
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)

    def run(self):
        '''
        @brief     This function runs the encoder task.
        @details   This function runs the task by taking a variety of inputs
                   and performing the tasks assigned to those inputs.
        @param     self.next_time
        @param     self.enc_Flag
        @return    This function returns the desired postion and delta.
        '''
        if utime.ticks_us() >= self.next_time:
            if self.enc_Flag.read() == 1:
                self.position_share.write(self.encoder.set_position(0))
            elif self.enc_Flag.read() == 2:
                self.delta_share.write(self.encoder.get_delta())
            else:
                self.encoder.update()
                self.position_share.write(self.encoder.get_position())
                self.delta_share.write(self.encoder.get_delta())
        
                self.next_time += self.period
                self.runs += 1
            